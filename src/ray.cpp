#include <cstdlib>
#include <iostream>
#include <ostream>

#include "camera.hpp"
#include "color.hpp"
#include "constants.hpp"
#include "hittable_list.hpp"
#define LODEPNG_COMPILE_DISK
#include "../lodepng/lodepng.hpp"
#include "sphere.hpp"

int main() {
	hittable_list world;

	// std::srand(static_cast<unsigned>(time(nullptr)));

	auto material_ground = make_shared<lambertian>(color(0.4, 0.8, 0.0));
	auto material_center = make_shared<lambertian>(color(0.1, 0.2, 0.5));
	auto material_left = make_shared<dielectric>(1.5);
	auto material_right = make_shared<metal>(color(0.8, 0.6, 0.2), 0.0);

	world.add(
		make_shared<sphere>(point3(0.0, -100.5, -1.0), 100.0, material_ground));
	world.add(
		make_shared<sphere>(point3(0.0, 0.0, -1.0), 0.5, material_center));
	world.add(make_shared<sphere>(point3(-1.0, 0.0, -1.0), 0.5, material_left));
	world.add(
		make_shared<sphere>(point3(-1.0, 0.0, -1.0), -0.4f, material_left));
	world.add(make_shared<sphere>(point3(1.0, 0.0, -1.0), 0.5, material_right));

	for (int i = 0; i < 200; ++i) {
		point3 center(random_double(-100.0, 15.0), random_double(-1.0, 1.001),
					  random_double(-10.0, -1.0));
		double radius = random_double(0.3, 1);

		// Randomly choose a material from the existing materials
		std::shared_ptr<material> random_material;
		double material_type = random_double();

		if (material_type < 0.25) {
			random_material = make_shared<lambertian>(
				color(random_double(), random_double(), random_double()));
		} else if (material_type < 0.45) {
			random_material = make_shared<dielectric>(random_double(1.0, 2.0));
		} else {
			random_material = make_shared<metal>(
				color(random_double(), random_double(), random_double()),
				random_double());
		}
		auto center2 = center + vec3(0, random_double(0, .5), 0);
		world.add(make_shared<sphere>(center, center2, 0.2, random_material));
		world.add(make_shared<sphere>(center, radius, random_material));
	}

	camera cam;
	cam.aspect_ratio = 16.0 / 9.0;
	cam.img_width = 400;
	cam.samples_per_pixel = 100;
	cam.max_depth = 50;

	cam.vertical_fov = 20;
	cam.look_from = point3(13, 2, 3);
	cam.look_at = point3(0, 0, 0);
	cam.vector_up = vec3(0, 1, 0);

	cam.defocus_angle = 0.02;
	cam.focus_distance = 10.0;

	std::vector<unsigned char> image = cam.render_png(world);

	std::string output_path = "output.png";
	if (lodepng::encode(output_path, image,
						static_cast<unsigned int>(cam.img_width),
						static_cast<unsigned int>(cam.img_height)) != 0) {
		// Error handling if the image fails to save.
		std::cerr << "Error saving PNG file." << std::endl;
		return EXIT_FAILURE;
	}

	std::cerr << "Image saved in " << output_path << std::endl;
	return EXIT_SUCCESS;
}