#pragma once
#include "constants.hpp"

class interval
{
public:
    double min, max;

    interval() : min(+inf), max(-inf) {}
    interval(double min, double max) : min(min), max(max) {}

    bool contains(double x) const
    {
        return min <= x && x <= max;
    }

    bool surround(double x) const
    {
        return min < x && x < max;
    }

    double clamp(double x) const {
        if (x < min) return min;
        if (x > max) return max;
        return x;
    }

    static const interval empty, universe;
};

const static interval empty (+inf, -inf);
const static interval universe(-inf, +inf);