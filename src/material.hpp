#include "color.hpp"
#include "constants.hpp"
#include "hittable.hpp"
#include "ray.hpp"

class hit_record;

class material {
   public:
	virtual ~material() = default;

	virtual bool scatter(const ray &ray_in, const hit_record &record,
						 color &attenuation, ray &scattered) const = 0;
};

class lambertian : public material {
   public:
	color albedo = vec3(0.0, 0.0, 0.0);
	lambertian(const color &a) : albedo(a) {}

	bool scatter(const ray &ray_in, const hit_record &record,
				 color &attenuation, ray &scattered) const override {
		ray_in.origin();
		vec3 scatter_direction = record.normal + random_unit_vector();
		if (scatter_direction.near_zero()) scatter_direction = record.normal;
		scattered = ray(record.p, scatter_direction, ray_in.time());
		attenuation = albedo;
		return true;
	}
};

class metal : public material {
   public:
	color albedo = vec3(0.0, 0.0, 0.0);
	double fuzz;
	metal(const color &a, double fuzz) : albedo(a), fuzz(fuzz) {}

	bool scatter(const ray &ray_in, const hit_record &record,
				 color &attenuation, ray &scattered) const override {
		vec3 reflected =
			reflect(unit_vector(ray_in.direction()), record.normal);
		scattered = ray(record.p, reflected + fuzz * random_unit_vector(),
						ray_in.time());
		attenuation = albedo;
		return dot(scattered.direction(), record.normal) > 0;
	}
};

class dielectric : public material {
   public:
	double index_of_refraction;
	dielectric(double index_of_refraction)
		: index_of_refraction(index_of_refraction) {}

	bool scatter(const ray &ray_in, const hit_record &record,
				 color &attenuation, ray &scattered) const override {
		attenuation = color(1.0, 1.0, 1.0);
		double refraction_ratio = record.front_face
									  ? (1.0 / index_of_refraction)
									  : index_of_refraction;
		vec3 unit_direction = unit_vector(ray_in.direction());
		double cos_theta = fmin(dot(-unit_direction, record.normal), 1.0);
		double sin_theta = sqrt(1.0 - pow(cos_theta, 2.0));

		bool cant_refract = refraction_ratio * sin_theta > 1.0;
		vec3 direction;

		if (cant_refract ||
			reflectance(cos_theta, refraction_ratio) > random_double())
			direction = reflect(unit_direction, record.normal);
		else
			direction =
				refract(unit_direction, record.normal, refraction_ratio);

		scattered = ray(record.p, direction, ray_in.time());
		return true;
	}

	static double reflectance(double cosine, double ref_index) {
		double r0 = (1.0 - ref_index) / (1.0 + ref_index);
		r0 = pow(r0, 2.0);
		return r0 + (1.0 - r0) * pow((1.0 - cosine), 5.0);
	}
};