#pragma once

#include "vec3.hpp"

class ray {
   private:
	point3 orig = vec3(0.0, 0.0, 0.0);
	vec3 dir = vec3(0.0, 0.0, 0.0);
	double current_t;

   public:
	ray() {}

	ray(const point3& origin, const vec3& direction, double time = 0.0)
		: orig(origin), dir(direction), current_t(time) {}

	point3 const origin() const { return orig; }
	vec3 direction() const { return dir; }
	double time() const { return current_t; }

	point3 at(double t) const { return orig + t * dir; }
};