#pragma once

#include <cmath>
#include <cstdlib>
#include <limits>
#include <memory>
#include <random>

using std::make_shared;
using std::shared_ptr;
using std::sqrt;

const double inf = std::numeric_limits<double>::infinity();
const double pi = 3.1415926535897932385;

inline double deg_to_rad(double degrees) { return degrees * pi / 180.0; }

inline double random_double() {
	double r = static_cast<double>(rand());
	double s = static_cast<double>(RAND_MAX) + 1.0;
	return r / s;
}

inline double random_double(double min, double max) {
	return min + (max - min) * random_double();
}

inline double rdm_double(double min, double max) {
	static std::random_device dev;
	static std::mt19937 rng(dev());
	static std::uniform_real_distribution<double> dist(min, max);
	return dist(rng);
}