#pragma once

#include <iostream>

#include "color.hpp"
#include "constants.hpp"
#include "hittable.hpp"
#include "material.hpp"
#include "vec3.hpp"

class camera {
   public:
	double aspect_ratio = 1.0;
	int img_width = 100;
	int img_height;
	int samples_per_pixel = 10;
	int max_depth = 10;
	int vertical_fov = 90;
	point3 look_from = point3(0.0, 0.0, -1.0);
	point3 look_at = point3(0.0, 0.0, 0.0);
	vec3 vector_up = vec3(0.0, 1.0, 0.0);
	vec3 u, v, w;
	double defocus_angle = 0.0;
	double focus_distance = 0.0;
	vec3 defocus_disk_u;
	vec3 defocus_disk_v;

	void render_ppm(const hittable &world) {
		init();

		std::cout << "P3\n" << img_width << ' ' << img_height << "\n255\n";
		for (int y = 0; y < img_height; y++) {
			std::clog << "\rScanlines remaining: " << (img_height - y) << ' '
					  << std::flush;
			for (int x = 0; x < img_width; x++) {
				color pixel_color(0.0, 0.0, 0.0);
				for (int i = 0; i < samples_per_pixel; i++) {
					ray r = get_ray(x, y);
					pixel_color += ray_color(r, max_depth, world);
				}
				write_color_out(std::cout, pixel_color, samples_per_pixel);
			}
		}
		std::clog << "\rDone.                   \n";
	}

	std::vector<unsigned char> render_png(const hittable &world) {
		init();
		std::vector<unsigned char> image;

		for (int y = 0; y < img_height; y++) {
			std::clog << "\rScanlines remaining: " << (img_height - y) << ' '
					  << std::flush;
			for (int x = 0; x < img_width; x++) {
				color pixel_color(0.0, 0.0, 0.0);
				for (int i = 0; i < samples_per_pixel; i++) {
					ray r = get_ray(x, y);
					pixel_color += ray_color(r, max_depth, world);
				}
				write_color(image, pixel_color, samples_per_pixel);
			}
		}
		std::clog << "\rDone.                   \n";
		return image;
	}

   private:
	point3 camera_center;
	point3 pixel00_location;
	vec3 pixel_delta_u;
	vec3 pixel_delta_v;

	void init() {
		img_height = static_cast<int>(img_width / aspect_ratio);
		img_height = (img_height < 1) ? 1 : img_height;

		camera_center = look_from;

		double theta = deg_to_rad(vertical_fov);
		double h = tan(theta / 2);
		double viewport_height = 2 * h * focus_distance;
		double viewport_width =
			viewport_height * (static_cast<double>(img_width) / img_height);

		w = unit_vector(look_from - look_at);
		u = unit_vector(cross(vector_up, w));
		v = cross(w, u);

		vec3 viewport_u = static_cast<double>(viewport_width) * u;
		vec3 viewport_v = static_cast<double>(viewport_height) * -v;

		pixel_delta_u = viewport_u / static_cast<double>(img_width);
		pixel_delta_v = viewport_v / static_cast<double>(img_height);

		vec3 viewport_upper_left = camera_center - (focus_distance * w) -
								   (viewport_u / 2.0) - (viewport_v / 2.0);
		pixel00_location =
			viewport_upper_left + 0.5 * (pixel_delta_u + pixel_delta_v);

		double defocus_radius =
			focus_distance * tan(deg_to_rad(defocus_angle / 2));
		defocus_disk_u = u * defocus_radius;
		defocus_disk_v = v * defocus_radius;
	}

	color ray_color(const ray &r, int depth, const hittable &world) {
		hit_record record;

		if (depth <= 0) {
			return color(0.0, 0.0, 0.0);
		}

		if (world.hit(r, interval(0.001, inf), record)) {
			ray scattered;
			color attenuation;
			if (record.mat->scatter(r, record, attenuation, scattered))
				return attenuation * ray_color(scattered, depth - 1, world);
			return color(0.0, 0.0, 0.0);

			vec3 direction = record.normal + random_unit_vector();
			return 0.1 * ray_color(ray(record.p, direction), depth - 1, world);
		}

		vec3 unit_direction = unit_vector(r.direction());
		double a = 0.5 * (unit_direction.y + 1.0);
		return (1.0 - a) * color(1.0, 1.0, 1.0) + a * color(0.5, 0.7, 1.0);
	}

	vec3 pixel_sample_square() const {
		double x = -0.5 + random_double();
		double y = -0.5 + random_double();
		return (x * pixel_delta_u) + (y * pixel_delta_v);
	}

	ray get_ray(int x, int y) {
		point3 pixel_center = pixel00_location +
							  (static_cast<double>(x) * pixel_delta_u) +
							  (static_cast<double>(y) * pixel_delta_v);
		vec3 pixel_sample = pixel_center + pixel_sample_square();
		vec3 ray_origin =
			(defocus_angle <= 0) ? camera_center : defocus_disk_sample();
		vec3 ray_direction = pixel_sample - ray_origin;
		double ray_time = random_double();

		return ray(ray_origin, ray_direction, ray_time);
	}

	point3 defocus_disk_sample() const {
		vec3 p = random_in_unit_disk();
		return camera_center + (p.x * defocus_disk_u) + (p.y * defocus_disk_v);
	}
};
