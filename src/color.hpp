#pragma once

#include <iostream>

#include "interval.hpp"
#include "vec3.hpp"

using color = vec3;

inline double linear_to_gamma(double linear_component) {
	return sqrt(linear_component);
}

inline void write_color_out(std::ostream &out, color pixel_color,
							int samples_per_pixel) {
	double r = pixel_color.x;
	double g = pixel_color.y;
	double b = pixel_color.z;

	double scale = 1.0 / static_cast<double>(samples_per_pixel);
	r *= scale;
	g *= scale;
	b *= scale;

	r = linear_to_gamma(r);
	g = linear_to_gamma(g);
	b = linear_to_gamma(b);

	static const interval intensity(0.000, 0.999);

	out << static_cast<int>(256 * intensity.clamp(r)) << ' '
		<< static_cast<int>(256 * intensity.clamp(g)) << ' '
		<< static_cast<int>(256 * intensity.clamp(b)) << '\n';
}

inline void write_color(std::vector<unsigned char> &image, color pixel_color,
						int samples_per_pixel) {
	double r = pixel_color.x;
	double g = pixel_color.y;
	double b = pixel_color.z;

	double scale = 1.0 / static_cast<double>(samples_per_pixel);
	r *= scale;
	g *= scale;
	b *= scale;

	r = linear_to_gamma(r);
	g = linear_to_gamma(g);
	b = linear_to_gamma(b);

	static const interval intensity(0.000, 0.999);

	image.insert(image.cend(),
				 static_cast<unsigned char>(256 * intensity.clamp(r)));
	image.insert(image.cend(),
				 static_cast<unsigned char>(256 * intensity.clamp(g)));
	image.insert(image.cend(),
				 static_cast<unsigned char>(256 * intensity.clamp(b)));
	image.insert(image.cend(), static_cast<unsigned char>(255));
}