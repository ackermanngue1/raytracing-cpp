#pragma once

#include "hittable.hpp"

#include <vector>


class hittable_list : public hittable
{
public:
    std::vector<shared_ptr<hittable>> objects;

    hittable_list() {}
    hittable_list(shared_ptr<hittable> object) { add(object); }

    void clear() { objects.clear(); }
    void add(shared_ptr<hittable> object)
    {
        objects.push_back(object);
    }

    bool hit(const ray &r, interval ray_t, hit_record &record) const override
    {
        hit_record tmp_record;
        bool hit_anything = false;
        double closest = ray_t.max;

        for (const shared_ptr<hittable> &object : objects)
        {
            if (object->hit(r, interval(ray_t.min, closest), tmp_record))
            {
                hit_anything = true;
                closest = tmp_record.t;
                record = tmp_record;
            }
        }

        return hit_anything;
    }
};