#pragma once

#include <memory>

#include "hittable.hpp"
#include "vec3.hpp"

class sphere : public hittable {
   public:
	point3 center = vec3(0.0, 0.0, 0.0);
	point3 center_1;
	double radius = 0.0;
	shared_ptr<material> mat;
	bool is_moving = false;
	vec3 center_vector;

	sphere(point3 center, double radius, shared_ptr<material> mat)
		: center(center), radius(radius), mat(mat) {}

	sphere(point3 center_1, point3 center_2, double radius,
		   shared_ptr<material> mat)
		: center_1(center_1), radius(radius), mat(mat), is_moving(true) {
		center_vector = center_2 - center_1;
	}

	bool hit(const ray &r, interval ray_t, hit_record &record) const override {
		point3 center = is_moving ? sphere_center(r.time()) : center_1;
		vec3 origin_sub_center = r.origin() - center;
		double a = r.direction().length_squared();
		double half_b = dot(origin_sub_center, r.direction());
		double c = origin_sub_center.length_squared() - radius * radius;

		double discriminant = half_b * half_b - a * c;
		if (discriminant < 0) return false;
		double sqrt_discriminant = sqrt(discriminant);

		double root = (-half_b - sqrt_discriminant) / a;
		if (!ray_t.surround(root)) {
			root = (-half_b + sqrt_discriminant) / a;
			if (!ray_t.contains(root)) return false;
		}

		record.t = root;
		record.p = r.at(record.t);
		vec3 outward_normal = (record.p - center) / radius;
		record.set_face_normal(r, outward_normal);
		record.mat = mat;

		return true;
	}

	point3 sphere_center(double time) const {
		return center + time * center_vector;
	}
};