#pragma once

#include <cmath>
#include <iostream>

#include "constants.hpp"

class vec3 {
   public:
	double x = 0.0;
	double y = 0.0;
	double z = 0.0;

	vec3() {}

	vec3(double x, double y, double z) : x(x), y(y), z(z) {}

	vec3 operator-() const { return vec3(-x, -y, -z); }

	vec3 &operator+=(const vec3 &v) {
		x += v.x;
		y += v.y;
		z += v.z;
		return *this;
	}

	vec3 &operator*=(const double t) {
		x *= t;
		y *= t;
		z *= t;
		return *this;
	}

	vec3 &operator/=(double t) { return *this *= 1 / t; }

	double length_squared() const {
		return pow(x, 2.0) + pow(y, 2.0) + pow(z, 2.0);
	}

	double length() const { return sqrt(length_squared()); }

	static vec3 random() {
		return vec3(random_double(), random_double(), random_double());
	}

	static vec3 random(double min, double max) {
		return vec3(random_double(min, max), random_double(min, max),
					random_double(min, max));
	}

	bool near_zero() const {
		double eps = 1e-8;
		return (fabs(x) < eps) && (fabs(y) < eps) && (fabs(z) < eps);
	}
};

using point3 = vec3;

inline std::ostream &operator<<(std::ostream &out, const vec3 &v) {
	return out << v.x << ' ' << v.y << ' ' << v.z;
}

inline vec3 operator+(const vec3 &u, const vec3 &v) {
	return vec3(u.x + v.x, u.y + v.y, u.z + v.z);
}

inline vec3 operator-(const vec3 &u, const vec3 &v) {
	return vec3(u.x - v.x, u.y - v.y, u.z - v.z);
}

inline vec3 operator*(const vec3 &u, const vec3 &v) {
	return vec3(u.x * v.x, u.y * v.y, u.z * v.z);
}

inline vec3 operator*(const vec3 &v, double t) {
	return vec3(v.x * t, v.y * t, v.z * t);
}

inline vec3 operator*(double t, const vec3 &v) { return v * t; }

inline vec3 operator/(vec3 v, double t) { return (1.0 / t) * v; }

inline double dot(const vec3 &u, const vec3 &v) {
	return u.x * v.x + u.y * v.y + u.z * v.z;
}

inline vec3 cross(const vec3 &u, const vec3 &v) {
	return vec3(u.y * v.z - u.z * v.y, u.z * v.x - u.x * v.z,
				u.x * v.y - u.y * v.x);
}

inline vec3 unit_vector(vec3 v) { return v / v.length(); }

inline vec3 random_in_unit_sphere() {
	while (true) {
		vec3 p = vec3::random(-1, 1);
		if (p.length_squared() < 1) return p;
	}
}

inline vec3 random_unit_vector() {
	return unit_vector(random_in_unit_sphere());
}

inline vec3 random_on_hemisphere(const vec3 &normal) {
	vec3 on_unit_sphere = random_unit_vector();
	if (dot(on_unit_sphere, normal) > 0.0)
		return on_unit_sphere;
	else
		return -on_unit_sphere;
}

inline vec3 reflect(const vec3 &v, const vec3 &n) {
	return v - 2 * dot(v, n) * n;
}

inline vec3 refract(const vec3 &uv, const vec3 &n, double eta_i_over_eta_t) {
	double cos_theta = fmin(dot(-uv, n), 1.0);
	vec3 r_out_perpendiculary = eta_i_over_eta_t * (uv + cos_theta * n);
	vec3 r_out_parallel =
		-sqrt(fabs(1.0 - r_out_perpendiculary.length_squared())) * n;
	return r_out_perpendiculary + r_out_parallel;
}

inline vec3 random_in_unit_disk() {
	while (true) {
		vec3 p = vec3(random_double(-1.0, 1.0), random_double(-1.0, 1.0), 0.0);
		if (p.length_squared() < 1.0) {
			return p;
		}
	}
}
