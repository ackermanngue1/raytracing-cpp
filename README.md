# Raytracing in C++

This repository is used for this course [https://raytracing.github.io/books/RayTracingInOneWeekend.html#overview]() rewritten by myself.

## Execution

### Run

In order to run the program and automaticaly compute the raytracing and store it into the _image.ppm_, use the following command : `make && make run`
`cmake --build build && ./build/raytracing > image.ppm`

### Debug

In order to run the program in debug mode so it will display the data that would be in the stdout : `make && make debug`
