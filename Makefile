CXX = g++
CXXFLAGS = -std=c++2a -Wall -Wextra

EXECUTABLE = raycast
IMAGE_OUTPUT = image.ppm

SRC = ray.cpp
HDR = camera.hpp, color.hpp, constants.hpp, hittable.hpp, hittable_list.hpp, interval.hpp, material.hpp, ray.hpp, sphere.hpp, vec3.hpp
OBJ = $(SRC:.cpp=.o)

all: $(EXECUTABLE)

$(EXECUTABLE): $(OBJ)
	$(CXX) $(CXXFLAGS) -o $@ $^

%.o: %.cpp $(HDR)
	$(CXX) $(CXXFLAGS) -c -o $@ $<

run: $(EXECUTABLE)
	./$(EXECUTABLE) > $(IMAGE_OUTPUT)

debug: $(EXECUTABLE)
	./$(EXECUTABLE)

clean:
	rm -f $(OBJ) $(EXECUTABLE)

.PHONY: all clean
